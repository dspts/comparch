def sdigit_dec_subtractor():
    """
    Single digit decimal subtractor
    --------------------------
    Works by taking two decimal digits as input and gradually decrements the minuend and subtrahend
    If the minuend is less than the subtrahend then the result is negative, so the carry is set
    In this case the result is given in the subtrahend but could use sdigit_dec_adder to move it to minuend
    """
    # Global variables
    minuend = 0
    subtrahend = 0
    carry = 0
    # Prompt user for two decimal digits
    minuend = int(input("Enter first digit: "))
    subtrahend = int(input("Enter second digit: "))
    # Decimal subtractor implementation
    while True:
        if minuend == 0 and subtrahend != 0:
            carry = 1
        if subtrahend == 0 or minuend == 0:
            print(f" Minuend: {minuend}, Subtrahend: {subtrahend}, Carry: {carry}")
            break
        minuend -= 1
        subtrahend -= 1


if __name__ == '__main__':
    sdigit_dec_subtractor()
