def sdigit_dec_adder():
    """
    Single digit decimal adder
    --------------------------
    Works by taking two decimal digits as input and gradually decrements the addend, while increments the accumulator (checking for overflow)
    When the addend is zero, the results are printed.
    """
    # Global variables
    addend = 0
    accumulator = 0
    carry = 0
    # Prompt user for two decimal digits
    addend = int(input("Enter first digit: "))
    accumulator = int(input("Enter second digit: "))
    # Decimal adder implementation
    while True:
        if addend == 0:
            print(f" Addend: {addend}, Accumulator: {accumulator}, Carry: {carry}")
            break
        
        addend -= 1
        accumulator = (accumulator+1)%10

        if accumulator == 0:
            carry += 1

if __name__ == '__main__':
    sdigit_dec_adder()
