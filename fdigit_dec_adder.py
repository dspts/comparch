def sdigit_dec_adder(addend, accumulator, carry=0):
    while True:
        if addend == 0:
            return (addend, accumulator, carry)
        
        addend -= 1
        accumulator = (accumulator+1)%10

        if accumulator == 0:
            carry += 1

def fdigit_dec_adder():
    addend = [int(x) for x in input("Enter first number (up to 40 digits): ").zfill(40)][::-1]
    accumulator = [int(x) for x in input("Enter second number (up to 40 digits): ").zfill(40)][::-1]
    carry = [0 for _ in range(40)]
    
    print("-- Initial 40 digit arrays --")
    print(f"Adden: {addend[::-1]}")
    print(f"Accum: {accumulator[::-1]}")
    print(f"Carry: {carry}")
    print()

    for counter in range(40):
        addend[counter], accumulator[counter], carry[counter] = sdigit_dec_adder(addend[counter], accumulator[counter])
    
    for counter in range(40-1):
        accumulator[counter + 1] = (accumulator[counter+1] + carry[counter])%10
        if (accumulator[counter+1] == 0):
            carry[counter+1] += carry[counter]
        carry[counter] = 0

    print("-- Final 40 digit arrays --")
    print(f"Adden: {addend[::-1]}")
    print(f"Accum: {accumulator[::-1]}")
    print(f"Carry: {carry[::-1]}")

if __name__ == "__main__":
    fdigit_dec_adder()
