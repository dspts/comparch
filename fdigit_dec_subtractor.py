def sdigit_dec_subtractor(minuend, subtrahend, carry=0):
    """
    Single digit decimal subtractor
    Works by gradually decrementing both minuend and subtrahend
    If minuend is 0 and subtrahend not then subtrahend is greater 
    """
    while True:
        if minuend == 0 and subtrahend != 0:
            carry += 1
            minuend += 9
            subtrahend -= 1
        if subtrahend == 0 or minuend == 0:
            return (minuend, subtrahend, carry)
        minuend -= 1
        subtrahend -= 1

def fdigit_dec_subtractor():
    """
    Forty digit decimal subtractor
    Uses the above single decimal subtractor to generate lists of minuend, subtrahend and carry and then adds carry where necessary
    When the minuend is less than the subtrahend, result is not optimized 
    """
    minuend = [int(x) for x in input("Enter first number (up to 40 digits): ").zfill(40)][::-1]
    subtrahend = [int(x) for x in input("Enter second number (up to 40 digits): ").zfill(40)][::-1]
    carry = [0 for _ in range(40)]
    print("-- Initial 40 digit arrays --")
    print(f"Minuend    : {minuend[::-1]}")
    print(f"Subtrahend : {subtrahend[::-1]}")
    print(f"Carry      : {carry}")
    print()
    for counter in range(40):
        minuend[counter], subtrahend[counter], carry[counter] = sdigit_dec_subtractor(minuend[counter], subtrahend[counter])
        if counter == 0:
            continue
        if carry[counter-1] == 1:
            minuend[counter], subtrahend[counter], carry[counter] = sdigit_dec_subtractor(minuend[counter], subtrahend[counter]+carry[counter-1])
            carry[counter-1] -= 1
    print("-- Final 40 digit arrays --")
    print(f"Minuend    : {minuend[::-1]}")
    print(f"Subtrahend : {subtrahend[::-1]}")
    print(f"Carry      : {carry[::-1]}")

if __name__ == "__main__":
    fdigit_dec_subtractor()
